extends Log
class_name QuestLog

# Constants

const QUESTS_DATA_FILE		= "res://Data/QuestsData/quests_data.json";
const OBJECTIVES_DATA_FILE	= "res://Data/QuestsData/objectives_data.json";

###########

# Variables

var completed_quests = 0;
var active_quest = -1;

###########


#QuestLog constructor
func _init(NAME : String = "").(NAME) -> void:
	self.set_quests_from_path(QUESTS_DATA_FILE);


#################################
#		QUESTS MANAGEMENT		#
#################################

# Adds a new quest to the QuestLog
func add_quest(quest : Quest) -> void:
	self.add_entry(quest);


# Removes a quest given its id
func remove_quest(quest_id : String) -> void:
	self.remove_entry(quest_id);


# Removes every quest from log
func clear_quests() -> void:
	self.clear_entries();


# Sets a quest as complete
func complete_quest(quest_id : String) -> void:
	if (Entries.has(quest_id) && self.is_quest_complete(quest_id) == false):
		self.Entries[quest_id].set_complete();
		self.completed_quests += 1;
		if (self.is_active(quest_id)):
			set_active_quest(""); #I HAVE TO THINK ABOUT IT


# Returns true if the given quest is complete
func is_quest_complete(quest_id : String) -> bool:
	if (Entries.has(quest_id) == false):
		print_debug("Quest not found: " + quest_id);
		return false;
	return self.Entries[quest_id].is_completed();


# Sets a quest as failed
func fail_quest(quest_id : String) -> void:
	if (Entries.has(quest_id) &&  self.is_quest_failed(quest_id) == false):
		self.Entries[quest_id].set_failed();
		if (self.is_active(quest_id)):
			set_active_quest("0"); #I HAVE TO THINK ABOUT IT


# Returns true if the given quest is failed
func is_quest_failed(quest_id : String) -> bool:
	if (Entries.has(quest_id) == false):
		print_debug("Quest not found: " + quest_id);
		return false;
	return self.Entries[quest_id].is_failed();


#################################
#		SETTERS AND GETTERS		#
#################################

# Sets the quest from an array of quest names
func set_quests(quests : Dictionary) -> void:
	if (quests.empty()):
		print_debug("The dictionary is empty.");
		print_debug("If you want to erase all the entries, use the method clear_quests()");
		print_debug("or the method clear_entries()");
		return;
	
	var objectives_data = GlobalVariables.parse_json_file(OBJECTIVES_DATA_FILE);
	if (objectives_data.has("Error")):
		print_debug(objectives_data["Error"]);
		return;
		
	for quest in quests.keys():
		var new_quest = Quest.new(quests[quest], objectives_data);
		if (new_quest != null):
			self.add_quest(quests[quest]);
		else:
			print_debug("An error occurred while trying to create quest " + quests[quest]["name"]);


# Sets the quest from a path of data files
func set_quests_from_path(PATH : String) -> void:
	var quests_data = GlobalVariables.parse_json_file(PATH);
	if (quests_data.has("Error")):
		print_debug(quests_data["Error"]);
		return;
	self.set_quests(quests_data);
	

# Sets a quest as active
func set_active_quest(quest_id : String) -> void:
	if (Entries.has(quest_id) == false):
		print_debug("Quest not found: " + quest_id);
		return;
	elif (self.Entries[quest_id].is_completed()):
		print_debug("Given quest has already been completed: " + quest_id);
		return;
	elif (self.Entries[quest_id].is_failed()):
		print_debug("Given quest has already been failed: " + quest_id);
		return;
	if (active_quest >= 0): #There can only be one active quest at once
		self.Entries[active_quest].set_inactive();
	self.Entries[quest_id].set_active();
	self.active_quest = quest_id;


# Returns an array containing the quests
func get_quests() -> Dictionary:
	return self.get_entries();


# Returns a quest given its index
func get_quest(quest_id : String) -> Quest:
	return self.get_entry(quest_id);


# Returns an array containing all the completed quests
func get_completed_quests() -> Array:
	var completed_quests = [];
	for quest in self.Entries.values():
		if (quest.is_completed()):
			completed_quests.append(quest);
	return completed_quests;


# Returns an array containing all the failed quests
func get_failed_quests() -> Array:
	var failed_quests = [];
	for quest in self.Entries.values():
		if (quest.is_failed()):
			failed_quests.append(quest);
	return failed_quests;


# Returns true if the given quest is the active quest
func is_active(quest_id : String) -> bool:
	return quest_id == active_quest;


# Returns the active quest index
func get_active_quest_id() -> int:
	return self.active_quest;


# Returns the active quest object
func get_active_quest() -> Quest:
	return self.get_quest(active_quest);


# Returns the number of completed quests
func get_completed_quests_number() -> int:
	return self.completed_quests;


# Returns the total number of quests
func get_quests_number() -> int:
	return self.get_entries_number();
